const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const User = require('../models/user.model.js');  

router.get('/', (req,res) => {
    
	User.find({}, function (err, allUsers) {
	    if (err) {
	        console.log(err);
	    } else {
	    	res.render('users/list', {
				Title : 'Users',
		    	viewTitle : 'User List',
		    	users: allUsers
			});
	    }
	})
});
router.get('/create', (req,res) => {
    res.render('users/create', {
        Title : 'Users',
        viewTitle : 'User Registeration'
    });
});

router.post('/', (req,res) => {
	insertEmployeeRecord(req, res);
});

function insertEmployeeRecord(req, res){

	var user = new User();
	user.first_name = req.body.first_name;
	user.last_name = req.body.last_name;
	user.email = req.body.email;
	user.mobile = req.body.mobile;
	user.city = req.body.city;
	user.state = req.body.state;
	user.password = req.body.password;
	user.save((err,doc) => {
		if (!err) {
			res.redirect('users/list');
		}else{
			console.log('error in insertion' + err);
		}
	});
}

router.get('/list', (req, res) => {

	User.find({}, function (err, allUsers) {
	    if (err) {
	        console.log(err);
	    } else {
	    	res.render('users/list', {
				Title : 'Users',
		    	viewTitle : 'User List',
		    	users: allUsers
			});
	    }
	})
});

router.get('/delete/:id', function(req, res) { 
    var uid = req.params.id.toString();

    User.deleteMany({"_id":uid}, function(err, result) { 
       console.log('Deleted');
	});
      res.redirect('/users/list');
});

module.exports = router;
  