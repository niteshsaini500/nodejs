const express = require('express');
var router = express.Router();

router.get('/', (req,res) => {

    var mascots = [
        { name: 'Nitesh', organization: "Information Technology", birth_year: 1995},
        { name: 'Aakash', organization: "Computer Engineer", birth_year: 2001},
        { name: 'Bhavya', organization: "Student", birth_year: 2020}
    ];
    var tagline = "No programming concept is complete without a cute animal mascot.";

    res.render('pages/dashboard', {
        mascots: mascots,
        tagline: tagline,
        Title : 'Dashboard',
		viewTitle : 'Dashboard'
    });
});

module.exports = router;