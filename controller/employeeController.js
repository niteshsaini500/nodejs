const express = require('express');
var router = express.Router();

const mongoose = require('mongoose');
const Employee = require('../models/employee.model.js');

// const Employee = mongoose.model('Employee', employeeSchema);


router.get('/', (req,res) => {
	res.render('employee/addOrEdit', {
		Title : 'Create Employee Form',
		viewTitle : 'Insert into the employee collection'
	});
});


router.post('/', (req,res) => {
	insertEmployeeRecord(req, res);
});

function insertEmployeeRecord(req, res){

	var employee = new Employee();
	employee.fullname = req.body.fullname;
	employee.email = req.body.email;
	employee.mobile = req.body.mobile;
	employee.city = req.body.city;
	employee.save((err,doc) => {
		if (!err) {
			// console.log('insert successfully');
			res.redirect('employee/list');
		}else{
			console.log('error in insertion' + err);
		}
	});
}


router.get('/list', (req, res) => {
	res.json('list');
});

module.exports = router;