require('./models/db');
var db = require('./models/db');
const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const ejs = require('ejs')
const bodyparser = require('body-parser');
 
const dashboardController = require('./controller/dashboardController');
const employeeController = require('./controller/employeeController');
const userController = require('./controller/userController');

var app = express();
app.use(bodyparser.urlencoded({
	extended : true
}));
app.use(bodyparser.json());

app.set('views', path.join(__dirname, '/views/'));
app.use(express.static(path.join(__dirname, 'public')));
// app.engine('hbs', exphbs({extname : 'hbs', defaultLayout : 'mainLayout', layoutsDir : __dirname + '/views/layouts/' }));
app.set('view engine', 'ejs');

app.listen(3000, () => {
	console.log('server started at 3000 port');
});


app.use('/dashboard', dashboardController);

app.use('/employee', employeeController);
app.use('/users', userController);


// // about page
// app.get('/about', function(req, res) {
//     res.render('pages/about');
// });



