const mongoose = require('mongoose')


var userSchema = new mongoose.Schema({

	first_name : {
		type : String
	},
	last_name : {
		type : String
	},
	email : {
		type : String
	},
	mobile : {
		type : String
	},
	city : {
		type : String
	},
	state : {
		type : String
	},
	password : {
		type : String
	}

});
module.exports=mongoose.model('user', userSchema);
// module.exports=mongoose.model('form', FormSchema);